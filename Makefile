.PHONY: all clean test

obj-m += time_stamp.o
time_stamp-objs := src/time.o

#KERNEL_DIR = /lib/modules/$(shell uname -r)/build
KERNEL_DIR = $(BUILD_KERNEL)

all:
	make -C $(KERNEL_DIR) M=$(PWD) modules

clean:
	make -C $(KERNEL_DIR) M=$(PWD) clean
test: all
	- @sshpass -p "1" scp -P 8022 -r ./* root@localhost:~/task07/
	- @sshpass -p "1" ssh -p 8022 root@localhost 'cd task07/ && ./test'
