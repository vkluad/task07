#include <linux/module.h>
#include <linux/types.h>
#include <linux/sysfs.h>
#include <linux/slab.h>
#include <linux/cdev.h>
#include <linux/kern_levels.h>
#include <linux/jiffies.h>

#define MODNAME "Time"

MODULE_LICENSE("GPL");


static u32 j;
static struct class *stime_class;
static bool mode;
struct timer_list my_timer;

void reverse(char s[])
{
	int i, j;
	char c;

	if (s == NULL)
		return;

	for (i = 0, j = strlen(s) - 1; i < j; i++, j--) {
		c = s[i];
		s[i] = s[j];
		s[j] = c;
	}
}

void itoa(long n, char s[])
{
	int i, sign;

	if (s == NULL)
		return;

	if ((sign = n) < 0)
		n = -n;
	i = 0;
	do {
		s[i++] = n % 10 + '0';
	} while ((n /= 10) > 0);
	if (sign < 0)
		s[i++] = '-';
	s[i] = '\0';
	reverse(s);
}

static ssize_t time_store(struct class *class, struct class_attribute *attr,
			  const char *buf, size_t count)
{
	if (strcmp(buf,"1\n") == 0)
		mode = 1;
	
	if (strcmp(buf,"0\n") == 0)
		mode = 0;

	return count;
}

static ssize_t time_show(struct class *class, struct class_attribute *attr,
			 char *buf)
{
	int temp_d, temp_h, temp_m, temp_s;
	char s_time[10];
	j = jiffies - j;
	
	if (mode){
		temp_d = (j / HZ) / (3600 * 24);
		itoa(temp_d, s_time);
		strncat(buf, s_time, strlen(s_time));
		strncat(buf, " Days ", 6);
		
		temp_h = ((j / HZ) - (temp_d * 24 * 3600)) / 3600;
		itoa(temp_h, s_time);
		if(strlen(s_time) < 2)
			strncat(buf, "0", 1);
		strncat(buf, s_time, strlen(s_time));
		strncat(buf, ":", 1);
		
		temp_m = ((j / HZ) - (temp_d * 24 * 3600) - temp_h * 3600) / 60;
		itoa(temp_m, s_time);
		if(strlen(s_time) < 2)
			strncat(buf, "0", 1);
		strncat(buf, s_time, strlen(s_time));
		strncat(buf, ":", 1);

		temp_s = (j / HZ) - (temp_d * 24 * 3600) - temp_h * 3600 - temp_m*60;
		itoa(temp_s, s_time);
		if(strlen(s_time) < 2)
			strncat(buf, "0", 1);
		strncat(buf, s_time, strlen(s_time));
		strncat(buf, " ago\n", 5);

	}else{
		itoa(j / HZ,s_time);
		strncpy(buf,s_time, strlen(s_time));
		strncat(buf, "s ago \n", 7);
	}
	j = jiffies;
	return strlen(buf);
}


CLASS_ATTR_RW(time);
			 
static int __init tinit( void )
{
	int res;
	j = jiffies;

	stime_class = class_create(THIS_MODULE, "mtime");
	res = class_create_file(stime_class, &class_attr_time);
	printk( KERN_NOTICE MODNAME ": MODULE INIT\n");

	return res;
}

static void __exit texit(void){
	class_remove_file(stime_class, &class_attr_time);
	class_destroy(stime_class);
}

module_init(tinit);
module_exit(texit);
