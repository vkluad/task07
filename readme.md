# Task 07


# [Task #7. Time Management](https://gl-khpi.gitlab.io/#practice-tasks)

---

## Description

Using time management in a kernel module.

---

## Guidance

> Use [Git](https://gl-khpi.gitlab.io/#commit-requirements) and the following names: `task07` - for your project's home directory; `partXX` - for the directory of each subtask (`XX` - subtask number); `src` - for the source code directory.

:one: Create a kernel module to take the time in seconds since the last read using *procfs/sysfs* interface. :ballot_box_with_check:

:two: Implement the ability to change the time format form seconds to `hh:mm:ss`. :ballot_box_with_check:

:three: Add the ability to get the absolute time in the format `hh:mm:ss`. :ballot_box_with_check:

:four: Additionally set the current time.

---

## Extra

Add the ability to get a random value in a given range every **N** seconds.


# Task07 result
```
4s ago 
0 Days 00:00:50 ago
3s ago 
```
